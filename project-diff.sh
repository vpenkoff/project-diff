#!/usr/bin/env bash

#
# project-diff
#
# Description
# project-diff is a small utility script that is used
# to compare two projects with similar directory and file
# structure. For example:
# given a project called PROJECT_A and a fork of the same
# project called PROJECT_B, we can compare what are the
# changes between them using the utility like:
# $ project-diff PROJECT_A/ PROJECT_B/
#
# The utility will create a directory called patches and
# will generate patch for each file diffs between the projects.
# Then one could easily apply them using the gnu patch command
# like patch -p1 < patches/file.patch
#

PROJECT_1=$1
PROJECT_2=$2

PATH_DEPTH="$(echo $2 | grep -o / | wc -l)"

mkdir -p patches
for i in `find ${PROJECT_1} -name '*.rb' | cut -d '/' -f $((${PATH_DEPTH}+1))-`; do
    echo "$(diff -ruaN ${PROJECT_1}$i ${PROJECT_2}$i)"
    `diff -ruaN ${PROJECT_1}$i ${PROJECT_2}$i > patches/${i##*/}.patch`
done

# Perform a cleanup
find patches/* -size 0c | xargs rm

echo "Completed..."
